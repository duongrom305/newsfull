<?php
    // require vendor css
    $vendorCSS = '
    <link rel="stylesheet" href="assets/vendor/DataTables/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="assets/vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="assets/vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="assets/vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">
    ';
    require_once 'layouts/header.php'?>
<?php
require_once '../lib/Session.class.php';
Session::checkSession();
Session::checkLogout();
?>
<div class="wrapper">

    <!-- Preloader -->
    <div class="preloader"></div>

    <?php require_once 'layouts/sidebar.php'; ?>

    <?php require_once 'layouts/nav-top.php'?>

    <div class="site-content">
        <!-- Content -->
        <div class="content-area py-1">
            <div class="container-fluid">
                <h4>Quản lý người dùng</h4>
                <ol class="breadcrumb no-bg mb-1">
                    <li class="breadcrumb-item active"></li>
                </ol>
                <div class="box box-block bg-white table-responsive">
                    <h5 class="mb-1">
                        <button class="btn btn-success btn-rounded" id="create-user">Thêm người dùng</button>
                    </h5>
                    <table class="table table-striped table-bordered dataTable" id="users" width="100%">
                        <thead>
                        <tr style="text-align: center;">
                            <th>#</th>
                            <th>Tên</th>
                            <th>Email</th>
                            <th>Vai trò</th>
                            <th width="20px">Ảnh</th>
                            <th>Ngày đăng ký</th>
                            <th width="100px">Hành động</th>
                        </tr>
                        </thead>
                        <tbody style="text-align: center;">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Modal view detail -->
<div class="modal animated bounceInUp default-modal small-modal" tabindex="-1" id="view-user" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="box bg-white user-1">
                    <div class="u-content">
                        <div class="avatar box-96 mt-2">
                            <img class="b-a-radius-circle" src="" alt="">
                            <i class="status bg-success bottom right"></i>
                        </div>
                        <h5 id="user-name"></h5>
                        <p id="user-email"></p>
                        <p id="user-role"></p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- Modal delete user -->
<div class="modal animated flipInX default-modal small-modal custom-modal-5" tabindex="-1" id="delete-user" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <div class="text-xs-center">
                    <div class="cm-icon bg-warning mb-1"><i class="ti-info"></i></div>
                    <p class="text-muted mb-0"></p>
                </div>
            </div>
            <div class="row no-gutter">
                <div class="col-xs-6">
                    <button type="button" id="btnYes" class="btn btn-primary btn-block">
                        Đồng ý
                    </button>
                </div>
                <div class="col-xs-6">
                    <button type="button" data-dismiss="modal" class="btn btn-secondary btn-block">
                        Hủy
                    </button>
                </div>
            </div>
        </div>

    </div>
</div>



<?php
    $vendorJS = '
    <script type="text/javascript" src="assets/vendor/DataTables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/vendor/DataTables/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="assets/vendor/DataTables/Responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="assets/vendor/DataTables/Responsive/js/responsive.bootstrap4.min.js"></script>
    <script type="text/javascript" src="assets/vendor/DataTables/Buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="assets/vendor/DataTables/Buttons/js/buttons.bootstrap4.min.js"></script>
    
    <script>
        $(document).ready(function(){
            var dataTable = $("#users").DataTable({
                "language": {
                    "lengthMenu": "Hiển thị _MENU_ mục",
                    "info": "Hiển thị _PAGE_ trên _PAGES_ trang.",
                    "infoEmpty": "Không có dữ liệu.",
                    "search":"Tìm kiếm",
                },
                "processing": true,
                "serverSide": true,
                "order": [],
                "ajax": {
                    url: "ajax/users/get-list-user.php",
                    method: "POST"
                }
            });
            // view detail user
            $("#view-user").on("show.bs.modal", function (event) {
                var button = $(event.relatedTarget);
                var id = button.data("id");
                var modal = $(this);
                $.ajax({
                    url: "ajax/users/view-user.php",
                    method: "POST",
                    dataType: "json",
                    data: {id: id},
                    success: function(data){
                        var img = modal.find("img");
                        var name = modal.find("#user-name");
                        var email = modal.find("#user-email");
                        var role = modal.find("#user-role");
                        img.attr("src",data.avatar);
                        name.text(data.name);
                        email.text(data.email);
                        role.text(data.role.toUpperCase());                        
                    }
                });
            });
            $("#delete-user").on("show.bs.modal", function (event) {
                var button = $(event.relatedTarget);
                var id = button.data("id");
                var name = button.data("name");
                var modal = $(this);
                modal.find("p").html("Bạn có muốn xóa <strong>"+name+"</strong> ?. Sau khi đồng ý bạn sẽ không khôi phục lại được.");
                $("#btnYes").on("click", function() {
                    $.ajax({
                        url:"ajax/users/delete-user.php",
                        method: "POST",
                        data: {id:id},
                        success: function() {
                            location.reload();
                        }
                    })
                });
            });
        });
    </script>';
?>
<?php require_once 'layouts/footer.php'; ?>
