<?php
    require_once '../lib/Session.class.php';
    Session::init();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Title -->
    <title>Neptune</title>

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="assets/vendor/bootstrap4/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/themify-icons/themify-icons.css">
    <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">

    <!-- Neptune CSS -->
    <link rel="stylesheet" href="assets/css/core.css">

</head>
<?php
    require_once '../config/config.php';
    require_once '../lib/Database.class.php';
    require_once '../helpers/Format.class.php';
    require_once 'class/User.class.php';
    if($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['btnLogin'])) {
        $data = [
            'email' => Format::validate($_POST['email']),
            'password' => Format::validate($_POST['password'])
        ];
        $user = new User();
        $messages = $user->login($data);
    }
?>
<body class="img-cover" style="background-image: url(assets/img/photos-1/2.jpg);">

<div class="container-fluid">
    <div class="sign-form">
        <div class="row">
            <div class="col-md-4 offset-md-4 px-3">
                <div class="box b-a-0">
                    <div class="p-2 text-xs-center">
                        <h5>Welcome</h5>
                    </div>
                    <?=isset($messages['loginFail'])?'<p class="text-danger text-sm-center">'.$messages['loginFail'].'</p>':'' ?>
                    <form class="form-material mb-1" action="" method="POST">
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Email">
                            <?=isset($messages['email'])?'<p class="text-danger ml-2">'.$messages['email'].'</p>':'' ?>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control form-control-danger" name="password" placeholder="Password">
                            <?=isset($messages['password'])?'<p class="text-danger ml-2">'.$messages['password'].'</p>':'' ?>
                        </div>
                        <div class="px-2 form-group mb-0 pb-2">
                            <button type="submit" class="btn btn-purple btn-block text-uppercase" name="btnLogin">Sign in</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Vendor JS -->
<script type="text/javascript" src="assets/vendor/jquery/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="assets/vendor/tether/js/tether.min.js"></script>
<script type="text/javascript" src="assets/vendor/bootstrap4/js/bootstrap.min.js"></script>
</body>
</html>
