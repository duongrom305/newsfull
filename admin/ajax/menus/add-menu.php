<?php
    require_once "../../../config/config.php";
    require_once "../../../lib/Database.class.php";
    require_once "../../class/Menu.class.php";
    require_once "../../../helpers/Format.class.php";
    if($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['name']){
        $data = [
            'name'=> Format::validate($_POST['name'])
        ];
        $mClass = new Menu();
        $message = $mClass->store($data);
        echo json_encode($message, JSON_UNESCAPED_UNICODE);
    }