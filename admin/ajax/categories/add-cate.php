<?php
    require_once '../../../config/config.php';
    require_once '../../../lib/Database.class.php';
    require_once '../../class/Category.class.php';
    require_once '../../../helpers/Format.class.php';
    require_once '../../../lib/Session.class.php';
    if($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['type'] == 'create'){
        $data = [
            'parent_id' => Format::validate($_POST['parent_id']),
            'name' => Format::validate($_POST['name']),
            'slug' => Format::slug($_POST['name'])
        ];
        $cate = new Category();
        $message = $cate->addCatgories($data);
        echo json_encode($message,JSON_UNESCAPED_UNICODE);
    }
?>