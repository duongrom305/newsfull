<?php
if($_SERVER['REQUEST_METHOD'] == 'GET'){
    header('Location: ../../dashboa\rd.php');
    exit();
}
require_once '../../../config/config.php';
require_once '../../class/User.class.php';
require_once '../../../helpers/Format.class.php';
require_once '../../../lib/Database.class.php';
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['id'])){
    $id = Format::validate($_POST['id']);
    $user = new User();
    $data = $user->viewUser($id);
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}
?>