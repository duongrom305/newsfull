<?php
    require_once '../../config/config.php';
    require_once '../class/User.class.php';
    require_once '../../helpers/Format.class.php';
    require_once '../../lib/Database.class.php';
    require_once '../../lib/Session.class.php';
    if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST)){
        $data = [
            'name' => Format::validate($_POST['name']),
            'email' => Format::validate($_POST['email']),
            'password' => '123456',
            'role' => Format::validate($_POST['role'])
        ];
        $user = new User();
        if(Session::isAdmin()){
            $messages = $user->addUser($data);
        }else{
            $messages = ['message'=>'unauthorized user'];
        }
        echo json_encode($messages,JSON_UNESCAPED_UNICODE);
    }
?>