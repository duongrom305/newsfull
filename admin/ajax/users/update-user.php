<?php
    require_once '../../../config/config.php';
    require_once '../../class/User.class.php';
    require_once '../../../helpers/Format.class.php';
    require_once '../../../lib/Database.class.php';
    require_once '../../../lib/Session.class.php';
    if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['btnUpdate'])){
        $data = [
            'id'=>Format::validate($_POST['id']),
            'name'=>Format::validate($_POST['name']),
            'email'=>Format::validate($_POST['email']),
            'role'=>Format::validate($_POST['role']),
        ];
        $user = new User();
        if (Session::isAdmin()){
            $messages = $user->updateUser($data);
        }else{
            $messages = ['message'=>'unauthorized user'];
        }
        echo json_encode($messages, JSON_UNESCAPED_UNICODE);
    }
?>