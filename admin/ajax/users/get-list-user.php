<?php
    require_once '../../../config/config.php';
    require_once '../../../lib/Database.class.php';
    require_once '../../class/User.class.php';
    require_once '../../../helpers/Format.class.php';
    require_once '../../../lib/Session.class.php';
    if($_SERVER['REQUEST_METHOD']  == 'POST'){
        $data = [];
        $data['search'] = Format::validate($_POST['search']['value']);
        $data['start'] = Format::validate($_POST['start']);
        $data['length'] = Format::validate($_POST['length']);
        if(isset($_POST['order'][0]['column'])) {
            $colums = [0=>'id',1=>'name',2=>'email',3=>'role',4=>'avatar',5=>'created_at',6=>'id'];
            $column = $colums[intval($_POST['order'][0]['column'])];
            $data['column'] = $column;
            $data['typeOrder'] = Format::validate($_POST['order'][0]['dir']);
        }
        $user = new User();
        $result = $user->getListUsers($data);
        $result['draw'] = $_POST['draw'];
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
?>