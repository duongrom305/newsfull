<?php
require_once '../../../config/config.php';
require_once '../../class/User.class.php';
require_once '../../../helpers/Format.class.php';
require_once '../../../lib/Database.class.php';
require_once '../../../lib/Session.class.php';
Session::init();
if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['id'])){
    $id = Format::validate($_POST['id']);
    $user = new User();
    if(Session::isAdmin()){
        if($user->deleteUser($id)){
            Session::setMessage('delete-user','Delete user success !');
        }else{
            Session::setMessage('delete-user','Delete user error !','error');
        }
    }else{
        Session::setMessage('delete-user','Unauthorized user !','error');
    }
}
?>