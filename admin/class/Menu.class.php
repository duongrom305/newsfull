<?php
    class Menu {
        private $db;
        public function __construct(){
            $this->db = new Database();
        }

        public function  index(){
            $sql = "SELECT id, name,  created_at FROM menus";
            $menus = $this->db->query($sql);
            return $menus;
        }

        /**
         * @param array $data
         * @return array
         */
        public function store(array $data){
            if($data){
                $errors = [];
                if(empty($data['name']))
                    $errors['name'] = 'Vui lòng nhập tên menu';
                if(!$errors){
                   $sql = 'INSERT INTO menus(name) VALUES(:name)';
                   $this->db->query($sql,$data);
                   return ['message'=>'created'];
                }
                return $errors;
            }
        }
    }