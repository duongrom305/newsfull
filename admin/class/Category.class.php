<?php
    class Category {
        private $db;

        public function __construct()
        {
            $this->db = new Database();
        }

        public function getListCategory(){
            $sql = 'SELECT id, name, parent_id FROM categories';
            return $this->db->query($sql);
        }

        public function showCategories($categories, $parent_id = 0, $str = ''){
            $stt = 0;
            foreach ($categories as $cate) {
                if ($cate['parent_id'] == $parent_id) {
                    echo '<tr>';
                        echo '<td width="10px">'.$cate["id"].'</td>';
                        if($str == ''){
                            echo '<td><strong>'.$cate["name"].'</strong></td>';
                        }else{
                            echo '<td>'.$str.' '.$cate["name"].'</td>';
                        }
                        echo
                            '<td>
                                </button>
                                <button type="button" data-id="'.$cate["id"].'" data-name="'.$cate["name"].'" data-parent="'.$cate["parent_id"].'" class="btn btn-warning btn-sm btn-circle waves-effect waves-light edit">
                                    <i class="ti-pencil"></i>
                                </button>
                                <button type="button" data-id="'.$cate["id"].'" data-name="'.$cate["name"].'" data-toggle="modal" data-target="#delete-cate" class="btn btn-danger btn-sm btn-circle waves-effect waves-light delete">
                                    <i class="ti-trash"></i>
                                </button>
                            </td>';
                    echo '</tr>';
                    $this->showCategories($categories, $cate['id'], $str.'__');
                }
            }
        }

        public function selectCategories($categories,$parent_id = 0, $str = ''){
            foreach ($categories as $cate){
                if($cate['parent_id'] == $parent_id){
                    echo '<option value="'.$cate['id'].'">'.$str.$cate['name'].'</option>';
                    $this->selectCategories($categories,$cate['id'],$str.'__');
                }
            }

        }

        public function addCatgories(array $data){
            $errors = [];
            if($data){
                if(empty($data['name'])){
                    $errors['name'] = 'Vui lòng nhập tên danh mục.';
                }
                if(!$errors){
                    $sql = 'INSERT INTO categories(name, slug, parent_id) VALUES(:name,:slug,:parent_id)';
                    $this->db->query($sql,$data);
                    return ['message'=>'created'];
                }
                return $errors;
            }
        }

        protected function checkChildren(array $data){
            $sql = 'SELECT * FROM categories WHERE parent_id=:id';
            return sizeof($this->db->query($sql, $data));
        }

        public function deleteCategories(array $data){
            $errors = [];

            if($data){
                if($this->checkChildren($data) != 0){
                    $errors['children'] = "Danh mục này tồn tại danh mục con vui lòng kiểm tra lại.";
                }else{
                    $sql = 'DELETE FROM categories WHERE id = :id';
                    $this->db->query($sql,$data);
                    return ['message'=>'deleted'];
                }
            }
            return $errors;
        }

        public function check(array $data){

        }

        public function updateCategories(array $data){
            $errors = [];
            if($data){
                if(empty($data['name'])){
                    $errors['name'] = 'Vui lòng nhập tên danh mục.';
                }
                if(!$errors){
                    $sql = 'UPDATE categories SET name = :name, slug = :slug, parent_id = :parent_id WHERE  id = :id';
                    $this->db->query($sql,$data);
                    return ['message'=>'updated'];
                }
                return $errors;
            }
        }
    }