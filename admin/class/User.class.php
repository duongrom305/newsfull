<?php

    class User {

        private $db;

        public function __construct()
        {
            $this->db = new Database();
        }

        public function login(array $data){
            $error = [];
            if($data){
                if(empty($data['email'])){
                    $error['email'] = 'Vui lòng nhập địa chỉ email.';
                }
                if(empty($data['password']) || strlen($data['password']) < 6){
                    $error['password'] = 'Mật khẩu phải có ít nhất 6 ký tự.';
                }else{
                    $data['password'] = sha1($data['password']);
                }
                if(!$error){
                    $sql = "SELECT name, avatar, email, role FROM users WHERE email = :email AND password = :password";
                    $user = $this->db->query($sql,$data);
                    if(sizeof($user) == 1){
                        Session::set('user', $user[0]);
                        header('Location: dashboard.php');
                    }else{
                        $error['loginFail'] = 'Thông tin đăng nhập không chính xác.';
                    }
                }
                return $error;
            }
        }

        public function getListUsers($data){
            $result = [];
            $temp = ['search'=>$data['search'].'%'];
            $sql = 'SELECT id, name, role, avatar, email, created_at FROM users ';
            if(isset($data['search'])){
                $sql .= 'WHERE id LIKE :search ';
                $sql .= 'OR name LIKE :search ';
                $sql .= 'OR email LIKE :search ';
                $sql .= 'OR role LIKE :search ';
                $sql .= 'OR created_at LIKE :search ';
            }
            if(isset($data['typeOrder'])){
                $sql .= 'ORDER BY '.$data['column'].' '.$data['typeOrder'].' ' ;
            }else{
                $sql .= 'ORDER BY id ASC ';
            }
            if($data['length'] != -1){
                $sql .= 'LIMIT '.$data['start'].', '.$data['length'].'';
            }
            $users = $this->db->query($sql,$temp);
            $size = sizeof($users);
            foreach ($users as $user){
                $avatars = '';
                if($user['avatar'] != ''){
                    $avatars = '<img class="b-a-radius-circle box-32" src="'.$user['avatar'].'" alt="">';
                }else{
                    $avatars = '';
                }
                $role = ucfirst($user['role']);
                if($user['role'] == 'admin'){
                    $role = "<span class='tag tag-pill tag-danger'>{$role}</span>";
                }else if ($user['role'] == 'author'){
                    $role = "<span class='tag tag-pill tag-primary'>{$role}</span>";
                }else {
                    $role = "<span class='tag tag-pill tag-warning'>{$role}</span>";
                }
                $temp = [];
                $temp[] = $user['id'];
                $temp[] = $user['name'];
                $temp[] = $user['email'];
                $temp[] = $role;
                $temp[] = $avatars;
                $temp[] = Format::fmDate($user['created_at']);
                $temp[] = ' <button type="button" data-id="'.$user["id"].'" data-toggle="modal" data-target="#view-user" class="btn btn-success btn-sm btn-circle waves-effect waves-light view">
                                <i class="ti-eye"></i>
						    </button>
						    <button type="button" data-id="'.$user["id"].'" class="btn btn-warning btn-sm btn-circle waves-effect waves-light edit">
                                <i class="ti-pencil"></i>
						    </button>
						    <button type="button" data-id="'.$user["id"].'" data-name="'.$user["name"].'" data-toggle="modal" data-target="#delete-user" class="btn btn-danger btn-sm btn-circle waves-effect waves-light delete">
                                <i class="ti-trash"></i>
						    </button>';
                $result[] = $temp;
            }
            return [
                'draw'=>'',
                'recordsTotal'=>$size,
                'recordsFiltered' => $this->getAllUser(),
                'data'=>$result
            ];
        }

        public function addUser(array $data){
            $error = [];
            if($data){
                $tempPassword = $data['password'];
                if(empty($data['name'])){
                    $error['name'] = 'Vui lòng nhập tên.';
                }
                if(empty($data['email'])){
                    $error['email'] = 'Vui lòng nhập địa chỉ email.';
                }
                if(!empty($data['password'])){
                    $data['password'] = sha1($data['password']);
                }
                if(empty($data['role'])){
                    $error['role'] = 'Vui lòng chọn quyền cho người dùng.';
                }
                if(!$error){
                    $sql = 'INSERT INTO users(name,email,password,role) VALUES(:name,:email,:password,:role)';
                    $this->db->query($sql,$data);
                    $result = [
                        'message' => 'created',
                        'id'=> $this->db->lastInsertId(),
                        'password' => $tempPassword
                    ];
                    return $result;
                }
                return $error;
            }
        }
        public function viewUser($id){
            if($id){
                $data = ['id'=>$id];
                $sql = "SELECT name, email, avatar, role FROM users WHERE id = :id";
                $user = $this->db->query($sql,$data);
                if($user){
                    return $user[0];
                }else{
                    return false;
                }
            }
        }

        public function updateUser(array $data){
            $error = [];
            if($data){
                if(empty($data['name'])){
                    $error['name'] = 'Vui lòng nhập tên.';
                }
                if(empty($data['email'])) {
                    $error['email'] = 'Vui lòng nhập địa chỉ email.';
                }
                if(empty($data['role'])){
                    $error['role'] = 'Vui lòng chọn quyền cho người dùng.';
                }
                if(!$error){
                    $sql = 'UPDATE users SET name = :name, email = :email, role= :role WHERE id = :id';
                    if($this->db->query($sql, $data)){
                        return ['message'=>'updated'];
                    }
                }
                return $error;
            }
        }

        public function deleteUser($id){
            if($id){
                $data = ['id'=>$id];
                $sql = 'DELETE FROM users WHERE id = :id';
                if($this->db->query($sql,$data)){
                    return true;
                }
                return false;
            }
        }
        public function getAllUser(){
            $sql = "SELECT * FROM users";
            return $this->db->rowCount($sql);
        }
    }
