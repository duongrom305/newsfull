<?php
// require vendor css
$vendorCSS = '
    <link rel="stylesheet" href="assets/vendor/DataTables/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="assets/vendor/DataTables/Responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="assets/vendor/DataTables/Buttons/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="assets/vendor/DataTables/Buttons/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="assets/vendor/dropify/dist/css/dropify.min.css">
    ';
require_once 'layouts/header.php'?>
<?php
require_once '../lib/Session.class.php';
Session::checkSession();
Session::checkLogout();
?>
<div class="wrapper">
    <!-- Preloader -->
    <div class="preloader"></div>
    <?php require_once 'layouts/sidebar.php'; ?>
    <?php require_once 'layouts/nav-top.php'?>
    <div class="site-content">
        <!-- Content -->
        <div class="content-area py-1">
            <div class="container-fluid">
                <h4>Quản lý menu</h4>
                <ol class="breadcrumb no-bg mb-1">
                    <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
                    <li class="breadcrumb-item active">Menus</li>
                </ol>
                <?php require_once '../config/config.php';
                    require_once '../lib/Database.class.php';
                    require_once '../helpers/Format.class.php';
                    require_once 'class/Menu.class.php'; ?>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="box box-block bg-white">
                            <button class="btn btn-success btn-rounded mb-2" id="create-menu">Thêm menu</button>
                            <table class="table table-striped table-bordered dataTable" id="menus">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Tên</th>
                                        <th>Ngày thêm</th>
                                        <th>Hành động</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php $mClass = new Menu(); $menus = $mClass->index(); ?>
                                <?php foreach ($menus as $menu): ?>
                                    <tr>
                                        <td><?php echo $menu['id'] ?></td>
                                        <td><?php echo $menu['name'] ?></td>
                                        <td><?php echo Format::fmDate($menu['created_at']) ?></td>
                                        <td>
                                            <a href="menu-items.php?id=<?php echo $menu['id'] ?>" class="btn btn-success btn-circle btn-sm"><i class="ti-list"></i></a>
                                            <button class="btn btn-warning btn-circle btn-sm"><i class="ti-pencil"></i></button>
                                            <button class="btn btn-danger btn-circle btn-sm"><i class="ti-trash"></i></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-block bg-white" hidden id="fMenu">
                            <div class="form-group">
                                <input type="text" class="form-control" id="mName" placeholder="Nhập tên menu">
                            </div>
                            <div class="form-group form-btn">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<?php
$vendorJS = '
    <script type="text/javascript" src="assets/vendor/DataTables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="assets/vendor/DataTables/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="assets/vendor/DataTables/Responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="assets/vendor/DataTables/Responsive/js/responsive.bootstrap4.min.js"></script>
    <script type="text/javascript" src="assets/vendor/DataTables/Buttons/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="assets/vendor/DataTables/Buttons/js/buttons.bootstrap4.min.js"></script>
    
    <script type="text/javascript">
        $("#menus").dataTable({
            "language": {
                    "lengthMenu": "Hiển thị _MENU_ mục",
                    "info": "Hiển thị _PAGE_ trên _PAGES_ trang.",
                    "infoEmpty": "Không có dữ liệu.",
                    "search":"Tìm kiếm",
                }
        });
        var fMenu = $("#fMenu");
        $("#create-menu").click(function(){          
            fMenu.attr("hidden",false);
            var btnAdd = $("<button>", {class:"btn btn-success btn-rounded", text:"Thêm"});
            fMenu.find(".form-btn").html(btnAdd);
            btnAdd.click(function() {
                var mName = $("#mName").val();
                $.post("ajax/menus/add-menu.php", {name: mName} ).done(function(rs){
                    rs = JSON.parse(rs);
                    if(rs.message === "created"){
                        toastr.success("Thêm menu thành công","Thành công !!");
                        setTimeout(function(){location.reload()},1000);    
                    }else
                      toastr.error(rs.message, "Lỗi !!");
               });
            });
        });
    </script>';
?>
<?php require_once 'layouts/footer.php'; ?>