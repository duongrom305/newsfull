<?php require_once 'layouts/header.php'?>
<?php
    require_once '../lib/Session.class.php';
    Session::checkSession();
    Session::checkLogout();
?>
<div class="wrapper">

    <!-- Preloader -->
    <div class="preloader"></div>

    <?php require_once 'layouts/sidebar.php'; ?>

    <?php require_once 'layouts/nav-top.php'?>

    <div class="site-content">
        <!-- Content -->
        <div class="content-area py-1">
            <div class="container-fluid">
                <h4>Dashboard</h4>
                <ol class="breadcrumb no-bg mb-1">
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>

            </div>
        </div>
    </div>

</div>
<?php require_once 'layouts/footer.php'; ?>