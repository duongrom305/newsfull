<!-- Sidebar -->
<div class="site-overlay"></div>
<div class="site-sidebar">
    <div class="custom-scroll custom-scroll-light">
        <ul class="sidebar-menu">
            <li>
                <a href="dashboard.php" class="waves-effect  waves-light">
                    <span class="s-icon"><i class="ti-anchor"></i></span>
                    <span class="s-text">Dashboard</span>
                </a>
            </li>
            <li class="menu-title">Management</li>
            <li>
                <a href="users.php" class="waves-effect  waves-light">
                    <span class="s-icon"><i class="ti-user"></i></span>
                    <span class="s-text">Users</span>
                </a>
            </li>
            <li>
                <a href="categories.php" class="waves-effect  waves-light">
                    <span class="s-icon"><i class="ti-list"></i></span>
                    <span class="s-text">Categories</span>
                </a>
            </li>
        </ul>
    </div>
</div>