<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Title -->
    <title>Neptune</title>

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="assets/vendor/bootstrap4/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/themify-icons/themify-icons.css">
    <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/vendor/pe7-icons/assets/Pe-icon-7-stroke.css">
    <link rel="stylesheet" href="assets/vendor/animate.css/animate.min.css">
    <link rel="stylesheet" href="assets/vendor/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" href="assets/vendor/waves/waves.min.css">
    <link rel="stylesheet" href="assets/vendor/switchery/dist/switchery.min.css">
    <link rel="stylesheet" href="assets/vendor/toastr/toastr.min.css">
    <?=isset($vendorCSS)?$vendorCSS:''?>
    <!-- Neptune CSS -->
    <link rel="stylesheet" href="assets/css/core.css">

</head>
<body class="fixed-sidebar fixed-header skin-2" >