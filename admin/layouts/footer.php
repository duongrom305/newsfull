    <!-- Vendor JS -->
        <script type="text/javascript" src="assets/vendor/jquery/jquery-1.12.3.min.js"></script>
        <script type="text/javascript" src="assets/vendor/tether/js/tether.min.js"></script>
        <script type="text/javascript" src="assets/vendor/bootstrap4/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/vendor/detectmobilebrowser/detectmobilebrowser.js"></script>
        <script type="text/javascript" src="assets/vendor/jscrollpane/jquery.mousewheel.js"></script>
        <script type="text/javascript" src="assets/vendor/jscrollpane/mwheelIntent.js"></script>
        <script type="text/javascript" src="assets/vendor/jscrollpane/jquery.jscrollpane.min.js"></script>
        <script type="text/javascript" src="assets/vendor/jquery-fullscreen-plugin/jquery.fullscreen-min.js"></script>
        <script type="text/javascript" src="assets/vendor/waves/waves.min.js"></script>
        <script type="text/javascript" src="assets/vendor/dropify/dist/js/dropify.min.js"></script>
        <script type="text/javascript" src="assets/vendor/toastr/toastr.min.js"></script>
        <?=isset($vendorJS)?$vendorJS:''?>

        <!-- Neptune JS -->
        <script type="text/javascript" src="assets/js/app.js"></script>
        <script type="text/javascript" src="assets/js/demo.js"></script>
    </body>
</html>