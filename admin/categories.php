<?php
// require vendor css
$vendorCSS = '
    <link rel="stylesheet" href="assets/vendor/nestable/nestable.css">
    ';
?>
<?php require_once 'layouts/header.php'?>
<?php
    require_once '../lib/Session.class.php';
    require_once '../config/config.php';
    require_once 'class/Category.class.php';
    require_once '../helpers/Format.class.php';
    require_once '../lib/Database.class.php';
    Session::checkSession();
    Session::checkLogout();
?>
<div class="wrapper">

    <!-- Preloader -->
    <div class="preloader"></div>

    <?php require_once 'layouts/sidebar.php'; ?>

    <?php require_once 'layouts/nav-top.php'?>

    <div class="site-content">
        <!-- Content -->
        <div class="content-area py-1">
            <div class="container-fluid">
                <h4>Quản lý danh mục</h4>
                <ol class="breadcrumb no-bg mb-1">
                    <li class="breadcrumb-item"><a href="">Home</a></li>
                    <li class="breadcrumb-item active">Categories</li>
                </ol>
                <h5 class="mb-1">
                    <button class="btn btn-success btn-rounded" id="btnCreateCate">Thêm danh mục</button>
                </h5>
                <div class="box box-block bg-white">
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="categories">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$vendorJS = '
    <script type="text/javascript" src="assets/vendor/nestable/jquery.nestable.js"></script>
    <script type="text/javascript">
    </script>';
?>
<?php require_once 'layouts/footer.php'; ?>
