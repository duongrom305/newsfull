<?php
    class Session {

        public static function init(){
            if(version_compare(phpversion(), '5.4.0', '<')){
                if(session_id()==''){
                    session_start();
                }
            }else{
                if(session_status()==PHP_SESSION_NONE){
                    session_start();
                }
            }
        }

        public static function set($key, $val){
            $_SESSION[$key] = $val;
        }

        public static function get($key){
            if(isset($_SESSION[$key])){
                return $_SESSION[$key];
            }else{
                return false;
            }
        }

        public static function checkSession(){
            self::init();
            if(!self::get('user')){
                self::destroy();
                header('Location: login.php');
            }
        }

        public static function checkLogout(){
            self::init();
            if($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['logout'])){
                self::destroy();
                header('Location: login.php');
            }
        }

        public static function isAdmin(){
            self::init();
            if(strtolower(self::get('user')['role'])=== 'admin'){
                return true;
            }
            return false;
        }

        public static function destroy(){
            session_destroy();
        }

        public static function hasMessage($type = 'success'){
        return (isset($_SESSION['flash'][$type]) && !empty($_SESSION['flash'][$type]))?true:false;
        }

        public static function setMessage($key, $val,$type = 'success'){
            $_SESSION['flash'][$type][$key] = $val;
        }

        public static function getMessage($key,$type = 'success'){
            if(isset($_SESSION['flash'][$type][$key])){
                return $_SESSION['flash'][$type][$key];
            }
            return NULL;
        }
        public static function  showMessage($key,$type = 'success'){
            if(isset($_SESSION['flash'][$type]) && array_key_exists($key,$_SESSION['flash'][$type])){
                return ($_SESSION['flash'][$type][$key]);
            }
        }
        public static function detroyMessage($key, $type = 'success'){
            unset($_SESSION['flash'][$type][$key]);
        }

    }