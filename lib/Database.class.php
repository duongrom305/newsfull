<?php
    /**
     * Developed by Duong Le
     * FB: https://www.facebook.com/duongrom.it.305
     */
    class Database {
        private $host = DB_HOST;
        private $user = DB_USER;
        private $pass = DB_PASS;
        private $db_name = DB_NAME;
        private $db_charset = DB_CHARSET;

        private $conn = null;

        public function __construct()
        {
            $this->connection();
        }
        /*
         * Connection Database.class
         * */
        public function connection(){
            try{
                if($this->conn === NULL){
                    $this->conn = new PDO("mysql:host=$this->host;dbname=$this->db_name;charset=$this->db_charset",$this->user,$this->pass);
                    $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    return $this->conn;
                }
            }catch (PDOException $e){
                echo "Connection failed: ".$e->getMessage();
                die();
            }
        }
        /*
         * @param string $sql
         * @param array $data
         * @return array
         * */
        public function query($sql,$data = array()){
            if(!$this->conn){$this->connection();}
            $type = $this->getType($sql);
            $stmt = $this->conn->prepare($sql);
            if($type === 'SELECT'){
                $data = $this->bindParam($data);
                $stmt->execute($data);
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                return $stmt->fetchAll();
            }else{
                return $stmt->execute($data);
            }
        }
        public function close(){
            $this->conn = NULL;
            return true;
        }
        /*
         * @param string $sql
         * @return string
         * */
        public function getType($sql){
            $type = trim($sql);
            $type = substr($type,0,6);
            $type = strtoupper($type);
            return $type;
        }

        public function bindParam(array $data){
            if($data){
                $temp = [];
                foreach ($data as $key => $val){
                    $temp[':'.$key] = $val;
                }
                return $temp;
            }
        }

        public function lastInsertId(){
            return $this->conn->lastInsertId();
        }

        public function  rowCount($sql){
            if(!$this->conn){$this->connection();}
            $stmt = $this->conn->prepare($sql);
            $stmt->execute();
            return $stmt->rowCount();
        }


    }