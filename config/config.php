<?php
    /**
     * Developed by Duong Le
     * FB: https://www.facebook.com/duongrom.it.305
     */
    define('DB_HOST','localhost');
    define('DB_USER','root');
    define('DB_PASS','');
    define('DB_NAME','db_news');
    define('DB_CHARSET','utf8');
