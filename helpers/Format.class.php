<?php

    class Format {

        public static function strRandom($length){
            $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charLength = strlen($char);
            $strRandom = '';
            for ($i = 0; $i < $length; $i++){
                $strRandom .= $char[rand(0, $charLength - 1)];
            }
            return $strRandom;
        }

        public static function fmDate($date){
            $date = strtotime($date);
            return date('H:i d/m/Y',$date);
        }

        public function textShorten($text, $limit = 400){
            $text = $text.' ';
            $text = substr($text,0, $limit);
            $text = substr($text,0, strripos($text,' '));
            $text = $text."...";
            return $text;
        }

        public static function validate($data){
            $data = trim($data);
            $data = strip_tags($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        public static function checkRole($role){
            $tmp = '';
            switch ($role){
                case 'admin':{
                    $tmp = 'bg-danger';
                    break;
                }
                case 'author':{
                    $tmp = 'bg-info';
                    break;
                }
                case 'editor':{
                    $tmp = 'bg-warning';
                    break;
                }
            }
            return '<span class="badge '.$tmp.'">'.ucfirst($role).'</span>';
        }

        public static function slug($str){
            $str = trim($str);
            $str = strtolower($str);
            $unicode = [
                'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
                'd'=>'đ',
                'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
                'i'=>'í|ì|ỉ|ĩ|ị',
                'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
                'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
                'y'=>'ý|ỳ|ỷ|ỹ|ỵ'
            ];
            foreach($unicode as $nonUnicode=>$uni){
                $str = preg_replace("/($uni)/i", $nonUnicode, $str);
            }
            $str = str_replace(" ", "-",$str);
            return $str;
        }

    }